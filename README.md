# AI / Big Data Projects




<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Apache_Spark_logo.svg/1200px-Apache_Spark_logo.svg.png" alt="mypy logo" width="100px"/>




### Notebooks



**Image recognition**


Given some X-ray images, use machine learning to help doctors detecting pneumonia.





**Recommander**


Collaborative filtering  - Alternating Least Squares matrix factorization.





**Customers Segmentation**


RFM Analysis (Recency, Frequency, Monetary)

